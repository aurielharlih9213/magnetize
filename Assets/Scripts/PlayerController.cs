﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	private Rigidbody2D rb2D;
	public float moveSpeed = 5.0f;
	public float pullForce = 100f;
	public float rotateSpeed = 360f;
	private GameObject closestTower;
	private GameObject hookedTower;
	private bool isPulled = false;
	private UIController uiControl;
	private AudioSource myAudio;
	private bool isCrashed = false;

    // Start is called before the first frame update
    void Start()
    {
		rb2D = this.gameObject.GetComponent<Rigidbody2D>();
		myAudio = this.gameObject.GetComponent<AudioSource>();
		uiControl = GameObject.Find("Canvas").GetComponent<UIController>();
    }

    // Update is called once per frame
    void Update()
    {
        // Move object
        rb2D.velocity = -transform.up * moveSpeed;

        if (Input.GetMouseButtonDown(0) && !isPulled) {
        	if (closestTower != null && hookedTower == null) {
        		hookedTower = closestTower;
        	}

        	if (hookedTower) {
        		float distance = Vector2.Distance(transform.position, hookedTower.transform.position);

        		// gravitation ke tower
        		Vector3 pullDirection = (hookedTower.transform.position - transform.position).normalized;
        		float newPullForce = Mathf.Clamp(pullForce / distance, 30, 100);
        		rb2D.AddForce(pullDirection * newPullForce);

        		// Angular velocity
        		rb2D.angularVelocity = -rotateSpeed / distance;
        		isPulled = true;
        	}
        }

        if (Input.GetMouseButtonUp(0)) {
        	rb2D.angularVelocity = 0;
        	isPulled = false;
        }

        if (isCrashed) {
        	if (!myAudio.isPlaying) {
        		restartPosition();
        	}
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
    	if (collision.gameObject.tag == "Wall") {
    		if (!isCrashed) {
    			// Play audio
    			myAudio.Play();
    			rb2D.velocity = new Vector3(0f,0f,0f);
    			rb2D.angularVelocity = 0f;
    			isCrashed = true;
    		}
    	}


    }

    public void OnTriggerEnter2D (Collider2D collision)
    {
       	if (collision.gameObject.tag == "Goal") {
    		Debug.Log("Finish");
    		uiControl.endGame();
    	}
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
    	if (collision.gameObject.tag == "Tower") {
    		closestTower = collision.gameObject;

    		// change tower color to green
    		collision.gameObject.GetComponent<SpriteRenderer>().color = Color.green;
    	}
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
    	if (isPulled) {
    		return;
    	}

    	if (collision.gameObject.tag == "Tower") {
    		closestTower = null;

    		// change color to normal
    		collision.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
    	}
    }

    public void restartPosition()
    {
    	var startPosition = new Vector3(-7f, 3f, 0f);
    	closestTower = null;
    	hookedTower = null;

    	// Set the start position
    	this.transform.position = startPosition;

    	// restart rotation
    	this.transform.rotation = Quaternion.Euler(-7f,0f,90f);

    	// isCrashed false
    	isCrashed = false;

    	if (closestTower) {
    		closestTower.GetComponent<SpriteRenderer>().color = Color.white;
    	}
    }
}
